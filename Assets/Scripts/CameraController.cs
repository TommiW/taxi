﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {


	public GameObject player;

	private Vector3 offset;
	private Vector3 newPosition;

	void Start () {
		newPosition = new Vector3(player.transform.position.x,player.transform.position.y,-10.0f);
		transform.position = newPosition;
	}

	// Update is called once per frame
	void Update () {
		
		newPosition = player.transform.position;
		newPosition.z = -10.0f;
		transform.position = newPosition;
	}
}
